package com.fahram.dicodingkade

/**
 * Created by fahram on 08/12/18
 */

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class AppGlideModule : AppGlideModule()