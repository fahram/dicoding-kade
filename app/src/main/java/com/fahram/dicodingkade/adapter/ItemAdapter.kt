package com.fahram.dicodingkade.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fahram.dicodingkade.GlideApp
import com.fahram.dicodingkade.R
import com.fahram.dicodingkade.activity.DetailActivity
import com.fahram.dicodingkade.model.Item
import com.fahram.dicodingkade.view.ItemUI
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity

class ItemAdapter(private val context: Context, private val items: List<Item>) :
    RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            context,
            ItemUI().createView(
                AnkoContext.create(
                    parent.context,
                    parent
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(private val context: Context, override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        private val name = containerView.find<TextView>(R.id.tvTitle)
        private val image = containerView.find<ImageView>(R.id.imgLogo)
        fun bindItem(items: Item) {

            name.text = items.name
            items.image?.let { GlideApp.with(itemView).load(it).into(image) }

            itemView.setOnClickListener {
                context.startActivity<DetailActivity>("item" to items)
            }
        }
    }
}