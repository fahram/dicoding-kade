package com.fahram.dicodingkade.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by fahram on 08/12/18
 */
@Parcelize
data class Item(val name: String?, val image: Int?, val description: String?) : Parcelable