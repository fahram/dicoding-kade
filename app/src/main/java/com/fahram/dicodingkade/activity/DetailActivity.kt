package com.fahram.dicodingkade.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.fahram.dicodingkade.GlideApp
import com.fahram.dicodingkade.R
import com.fahram.dicodingkade.model.Item
import com.fahram.dicodingkade.view.DetailActivityUI
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ui = DetailActivityUI()
        ui.setContentView(this)
        val item = intent.getParcelableExtra<Item>("item")
        val title = find<TextView>(R.id.titleClub)
        val descriptionClub = find<TextView>(R.id.descriptionClub)
        val image = find<ImageView>(R.id.imgClub)
        title.text = item.name
        descriptionClub.text = item.description
        item.image?.let { GlideApp.with(this).load(it).into(image) }
    }
}
