package com.fahram.dicodingkade.view

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.fahram.dicodingkade.activity.MainActivity
import com.fahram.dicodingkade.adapter.ItemAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * Created by fahram on 08/12/18
 */

class MainActivityUI(val mAdapter: ItemAdapter) : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        return relativeLayout {
            padding = dip(16)
            lparams(width = matchParent, height = wrapContent)
            recyclerView {
                lparams(width = matchParent, height = wrapContent)
                layoutManager = LinearLayoutManager(ctx)
                adapter = mAdapter
            }
        }
    }
}