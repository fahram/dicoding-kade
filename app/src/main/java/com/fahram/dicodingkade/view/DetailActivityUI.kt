package com.fahram.dicodingkade.view

import android.view.Gravity
import com.fahram.dicodingkade.activity.DetailActivity
import com.fahram.dicodingkade.R
import org.jetbrains.anko.*

/**
 * Created by fahram on 08/12/18
 */
class DetailActivityUI : AnkoComponent<DetailActivity> {

    override fun createView(ui: AnkoContext<DetailActivity>) = with(ui) {


        verticalLayout {

            imageView {
                id = R.id.imgClub
                R.drawable.img_barca
            }.lparams(height = 200) {
                padding = dip(20)
                margin = dip(15)
                gravity = Gravity.CENTER_HORIZONTAL
            }

            textView {
                id = R.id.titleClub
            }.lparams(width = wrapContent) {
                padding = dip(20)
                margin = dip(15)
                gravity = Gravity.CENTER_HORIZONTAL
            }

            textView {
                id = R.id.descriptionClub
            }.lparams(width = wrapContent) {
                padding = dip(20)
                margin = dip(15)
            }

        }
    }
}