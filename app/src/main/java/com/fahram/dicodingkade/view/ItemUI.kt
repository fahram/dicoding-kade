package com.fahram.dicodingkade.view

import android.view.*
import android.widget.*
import com.fahram.dicodingkade.R
import org.jetbrains.anko.*

class ItemUI : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {

        linearLayout {
            orientation = LinearLayout.HORIZONTAL
            padding = dip(16)

            lparams(width = matchParent)
            imageView {
                id = R.id.imgLogo
                R.mipmap.ic_launcher
            }.lparams(width = dip(50), height = dip(50))

            textView {
                id = R.id.tvTitle
            }.lparams(width = wrapContent, height = wrapContent) {
                gravity = Gravity.CENTER_VERTICAL
                margin = dip(10)
            }
        }
    }

}